
\documentclass{article}
\usepackage[utf8]{inputenc}
\usepackage[sort,comma,square,numbers]{natbib}
%\usepackage[notcite,notref]{showkeys}
\usepackage[hyphens]{url}
\usepackage[bookmarks=false, colorlinks, linkcolor=red!60!black, pagebackref]{hyperref}
\usepackage{microtype}
\usepackage{xcolor}
\usepackage{amsmath}
\usepackage{amsthm}
\usepackage{mathtools}
\usepackage{paralist}
\usepackage{amssymb}
\usepackage[textsize=scriptsize]{todonotes}
\usepackage{pdflscape}
\usepackage{tikz}
\usetikzlibrary{positioning}
\usepackage{graphicx}
\DeclareGraphicsRule{*}{mps}{*}{} 


\newtheorem{theorem}{Theorem}[section]
\newtheorem{lemma}{Lemma}[section]
\newtheorem{corollary}{Corollary}[section]
\newtheorem{rrule}{Reduction Rule}[section]
\newtheorem{observation}{Observation}[section]

\theoremstyle{definition}
\newtheorem{definition}{Definition}[section]
\newtheorem{construction}{Construction}[section]
\newtheorem{transformation}{Transformation}[section]
\newtheorem{example}{Example}[section]

\newcommand{\observationautorefname}{Observation}
\newcommand{\exampleautorefname}{Example}
\newcommand{\transformationautorefname}{Transformation}
\newcommand{\lemmaautorefname}{Lemma}
\newcommand{\corollaryautorefname}{Corollary}
\newcommand{\constructionautorefname}{Construction}
\newcommand{\rruleautorefname}{Reduction Rule}

\renewcommand{\bibsection}{\section*{Bibliography}}

% definitions
\def\N{\ensuremath{\mathbb{N}}}
\def\G{\ensuremath{\mathcal{G}}}
\def\mid{:}


%max-leaf number
\def\ml{\ensuremath{\mathrm{ml}}}
% bandwidth
\def\bw{\ensuremath{\mathrm{bw}}}
% girth
\def\girth{\ensuremath{gi}}
% chromatic number
\def\cn{\ensuremath{\chi}}
% acyclic chromatic number
\def\acn{\ensuremath{\cn_a}}
% boxicity
\def\boxicity{\ensuremath{b}}
% max degree
\def\mxd{\ensuremath{\Delta}}
% genus
\def\gen{\ensuremath{g}}
% edge biclique number
\def\eb{\ensuremath{\mathrm{eb}}}
% biclique number
\def\bn{\ensuremath{\mathrm{bn}}}


% Shorthands

\title{The Graph Parameter Hierarchy}

\begin{document}

\maketitle{}

\section{About}
The purpose of this document is to gather graph parameters (also called graph invariants) and their relationships in a central place. It was edited by Manuel Sorge\footnote{\url{manuel.sorge@gmail.com}} and Mathias Weller\footnote{\url{mathias.weller@u-pem.fr}} with contributions by René van Bevern, Florent Foucaud, Ondřej Suchý, Pascal Ochem, Martin Vatshelle, and Gerhard J. Woeginger. This project and the diagram shown in \autoref{fig:diag} is inspired by the work of Bart M. P. Jansen~\cite{Jan13}. Some further related work (in no particular order) is the Information System on Graph Classes and their Inclusions (ISGCI)\footnote{\url{http://www.graphclasses.org}}, the Graph Parameter Project~\cite{Sas10}, INGRID: a graph invariant manipulator~\cite{DBG89} and the work by Martin Vatshelle~\cite{Vat12}.
 
\section{Terminology}
\newcommand{\pargeq}{\ensuremath{\succeq}}
A \emph{graph parameter} is a function~$\phi \colon \mathbb{G} \to \mathbb{R}$ where~$\mathbb{G}$ is the set of finite graphs and~$\mathbb{R}$ is the set of real numbers. Let~$\phi$, $\psi$ graph parameters. The parameter~$\phi$ \emph{upper bounds} another parameter~$\psi$, if there is some function~$f$ such that for every graph~$G$ in~$\mathbb{G}$ it holds that~$\psi(G) \leq f(\phi(G))$; we write~$\phi \pargeq \psi$. Parameter~$\phi$ \emph{is unbounded in}~$\psi$ if~$\neg (\psi \pargeq \phi)$. Parameter~$\phi$ \emph{strictly upper bounds}~$\psi$ if~$\phi \pargeq \psi \wedge \neg (\psi \pargeq \phi)$. If~$(\neg (\phi \pargeq \psi)) \wedge \neg (\psi \pargeq \phi)$ then~$\phi$ and~$\psi$ are \emph{incomparable}. If $\phi \pargeq \psi \wedge \psi \pargeq \phi$ then~$\phi$ and~$\psi$ are \emph{equal}.

\begin{landscape}
\begin{figure}
  \resizebox{\linewidth}{!}{%
    \input{graph_params.tex}
  }
  \caption{Hasse diagram of the known part of the boundedness relation between graph parameters.}\label{fig:diag}
\end{figure}
\end{landscape}
%Hasse diagram of the strict boundedness relation between graph parameters: a parameter~$\phi$ \emph{strictly upper bounds} another parameter~$\psi$, if there is some function~$f$ such that~$\psi \leq f(\phi)$ for every graph but there is no function~$g$ such that~$\phi \leq g(\psi)$ for every graph.


\section{Parameter Definitions}
% Please use alphabetical order and give a reference to a definition.

\subsection{Acyclic Chromatic Number}
The \emph{acyclic chromatic number} of a graph~$G = (V, E)$ is the smallest size of a vertex partition~$P = \{V_1, \ldots, V_\ell\}$ such that each~$V_i$ is an independent set and for all~$V_i, V_j$ the graph~$G[V_i \cup V_j]$ does not contain a cycle. In other words, the acyclic chromatic number is the smallest number of colors needed for a proper vertex coloring such that every two-chromatic subgraph is acyclic. 

Introduced by~\citet{Gru73}.


\subsection{Covering Parameters}

\subsubsection{Path Number}
The \emph{path number} of a graph~$G$ is the minimum number of paths the edges of~$G$ can be partitioned into~\cite{AEH80}. Exists in ``disjoint'' and ``overlapping'' versions where the paths have to be disjoint or not, respectively.

\subsubsection{Arboricity}
The \emph{arboricity} of a graph~$G$ is the minimum number of forests the edges of~$G$ can be partitioned into.
It is called \emph{linear arboricity} if the forests are linear (collection of paths).

\subsubsection{Vertex Arboricity}
The \emph{vertex arboricity} (or ``point arboricity'') of a graph~$G$ is the minimum number of vertex subsets~$V_i$ of~$G$ such that $G[V_i]$ induces a forest for each~$i$.
It is called \emph{linear vertex arboricity} if the forests are linear (collection of paths).
If~$G$ is the line graph of~$G'$, then this equals the (linear) arboricity of~$G'$~\cite{AEH80}.

\subsubsection{Average Degree}
The \emph{average degree} of a graph~$G = (V, E)$ is~$2|E|/|V|$.


\subsection{Graph Intersection Parameters}
Let $\G$ be a class of graphs and let $G=(V,E)$ not necessarily in $\G$.
Let $p$ be the smallest number $p$ of sets $E_i$ with $E=\bigcap_{i\leq p}E_i$ and each $(V,E_i)\in\G$.
Then, $p$ is called $G$'s \emph{$\G$-intersection number}.

\subsubsection{Interval-Intersection (Boxicity)}
The \emph{boxicity} is the $\G$-intersection number for $\G$ being the class of interval graphs.
Exceptionally, each clique has boxicity~$0$.

An equivalent alternative definition is the following.
% Taken from \cite{CS07}
An axis-parallel $b$-dimensional box is a Cartesian product $R_1\times R_2 \times \ldots \times R_b$ where $R_i$ (for $1 \leq i \leq b$) is a closed interval of the form $[a_i, b_i]$ on the real line. For a graph~$G$, its \emph{boxicity} is the minimum dimension $b$ such that $G$ is representable as the intersection graph of (axis-parallel) boxes in $b$-dimensional space.

\subsubsection{Chordal-Intersection (Chordality)}
The \emph{chordality} is the $\G$-intersection number for $\G$ being the class of chordal graphs.

\subsection{Average Distance}
The \emph{average distance} of a graph~$G = (V, E)$ is~$1/\binom{n}{2} \cdot \sum_{u, v \in V} d(u, v)$, where~$d(u, v)$ is the length of a shortest path between~$u$ and~$v$ in~$G$.


\subsection{Bandwidth}
The \emph{bandwidth}~$\bw$ of a graph~$G$ is the maximum ``length'' of an edge in a one dimensional layout of~$G$.
Formally:
\[
  \ml := \min_{i:V\rightarrow\N}\{\max_{\{u,v\}\in E}\{|i(u)-i(v)|\}\mid\text{$i$ is injective}\}
\]

\subsection{Bisection Width}
The width of a bipartition of a graph is the number of edges going between the parts. The \emph{bisection width} of a graph~$G = (V, E)$ is the smallest width of a bipartition of~$G$ such that the difference of the parts' numbers of vertices is at most one.


\subsection{Branchwidth}
A \emph{branch decomposition} of a hypergraph~$H = (V, \mathcal{F})$ is a tuple~$(T, \tau)$, where~$T$ is a rooted binary tree and where~$\tau$ is a bijection from the leaves of~$T$ to the hyperedges~$\mathcal{F}$. The \emph{order} of an edge~$e$ in~$T$ is the number of vertices~$v$ in~$H$ such that there are leaves~$t_1, t_2$ in different connected components of~$T \setminus e$ for which~$\tau(t_1), \tau(t_2)$ both are incident to~$v$. The \emph{width} of a branch decomposition is the maximum order of edges in~$T$. The \emph{branchwidth} of~$H$ is the minimum width of branch-decompositions of~$H$.

\subsection{Chromatic Number}
The \emph{chromatic number}~$\cn$ of a graph~$G$ is the smallest number~$i$ such that the the vertices of~$G$ can be partitioned into~$i$ independent sets. 

\subsection{Cliquewidth}
% Ondra Suchy wrote this definition. Copied from balbal project.
\newcommand{\intro}{\bullet}
\newcommand{\join}{\eta}
\newcommand{\recol}{\rho}
\newcommand{\union}{\oplus}
Let $q$ be a positive integer. We call $(G,\lambda)$ a \emph{$q$-labeled graph} if $G$ is a graph and $\lambda: V(G) \to
\{1,2,\ldots,q\}$ is a mapping. The number $\lambda(v)$ is called \emph{label} of a vertex $v$. We introduce the following operations on labeled graphs:
\begin{itemize}
\item[(1)] For every $i$ in $\{1,\ldots,q\}$, we let $\intro_i$ denote the graph with only one vertex that is labeled by $i$ (a constant operation).
\item[(2)] For every distinct $i$ and $j$ from $\{1,2,\ldots,q\}$, we define a unary operator $\join_{i,j}$ such that
$\join_{i,j}(G, \lambda) = (G', \lambda)$, where $V(G') = V(G)$, and $E(G') = E(G) \cup \{vw \mid v,w \in V, \lambda(v) =
i, \lambda(w) = j\}$. In other words, the operator adds all edges between label-$i$ vertices and label-$j$ vertices.
\item[(3)] For every distinct $i$ and $j$ from $\{1,2,\ldots,q\}$, we let $\recol_{i \to j}$ be the unary operator such that $\recol_{i \to j}(G, \lambda) = (G, \lambda')$, where $\lambda'(v) = j$ if $\lambda(v) = i$, and $\lambda'(v)=\lambda(v)$ otherwise.
The operator only changes the labeling so that the vertices that originally had label $i$ will now have label $j$.
\item[(4)] Finally, $\union$ is a binary operation that makes the disjoint union, while keeping the labels of the vertices  unchanged. Note explicitly that the union is disjoint in the sense that $(G, \lambda) \union (G, \lambda)$ has twice the number of vertices of $G$.
\end{itemize}
A \emph{$q$-expression} is a well-formed expression $\varphi$ written with these symbols. The
$q$-labeled graph produced by performing these operations in order therefore has a vertex for each occurrence of the constant symbol in $\varphi$; and this $q$-labeled graph (and
any $q$-labeled graph isomorphic to it) is called the \emph{value} $val(\varphi)$ of $\varphi$. If a $q$-expression $\varphi$
has value $(G, \lambda)$, we say that $\varphi$ is a \emph{$q$-expression of} $G$. The \emph{cliquewidth} of a
graph $G$, denoted by $cwd(G)$, is the minimum $q$ such that there is a $q$-expression of $G$.

Cliquewidth has been defined by~\citet{CO00}. The definition above is inspired by \citet{HOSG08}.

\subsection{Clique Cover}
A \emph{clique cover} of a graph~$G = (V, E)$ is a partition~$P$ of~$V$ such that each part in~$P$ induces a clique in~$G$. The \emph{minimum clique cover} of~$G$ is a clique cover with a minimum number of parts.
Note that the clique cover number of a graph is exactly the chromatic number of its complement.

\subsection{Coloring Number}
Is one larger than the degeneracy. Introduced by \citet{EH66}.

\subsection{Degeneracy}
The \emph{degeneracy} of a graph~$G$ is the maximum, with respect to all subgraphs~$G'$ of~$G$, of the minimum degree of~$G'$. Equivalent definitions include the minimum outdegree over all acyclic orientations of~$G$ and the minimum, over all linear ordering so the vertices, of the maximum, over all vertices~$v$, of the number of neighbors of~$v$ that occur later in the ordering.

\subsection{Density}
Also known as average degree.

\subsection{Distance to \boldmath$\Pi$}
The \emph{distance to $\Pi$} of a graph~$G=(V,E)$ is the minimum size of a set~$X\subseteq V$ such that $G[V\setminus X]\in\Pi$, where $\Pi$ is a graph class, e.\,g.\ chordal, bipartite, or clique.
%\subsection{Edge Biclique Number}
%The \emph{Edge Biclique Number}~$\eb$ of a graph~$G$ is the maximum number of edges in~$G'$ over all complete bipartite subgraphs~$G'$ of~$G$.

\subsection{Domatic Number}
The \emph{domatic number} of a graph~$G$ is the maximum number of pairwise disjoint dominating sets of~$G$.

\subsection{\boldmath$h$-Index}
The~\emph{$h$-index} of a graph~$G$ is the maximum integer~$h$ such that~$G$ contains $h$ vertices of degree at least $h$.


\subsection{Genus}
The \emph{genus}~$\gen$ of a graph~$G$ is the minimum number of handles over all surfaces on which~$G$ can be embedded.

\subsection{Girth}
The \emph{girth} of a graph~$G$ is the minimum number~$\girth$ such that~$G$ has a cycle of length~$\girth$.

\subsection{Linkage}
Also known as degeneracy. Introduced by \citet{KT96}.

\subsection{Max Leaf Number}
The \emph{max leaf number}~$\ml$ of a graph~$G$ is the maximum number of leaves in a spanning tree of~$G$.

\subsection{Subgraph-Cutset-Number}
The \emph{subgraph-cutset-number} is the maximum, with respect to all subgraphs~$G'$ of~$G$, of the minimum vertex cut of~$G'$. Here, a vertex cut in a graph~$G$ is a set of vertices such that removing them from~$G$ yields a disconnected graph; considering graphs that contain only a single vertex also as disconnected.

\subsection{Treedepth}
Let the \emph{closure} of a rooted tree be the graph constructed from
that tree by adding an edge~$\{u, v\}$, for each pair of
vertices~$u, v$ where $u$ is an ancestor of~$v$. The \emph{treedepth}
of a graph~$G$ is the smallest height of a rooted tree~$T$ such
that~$G$ is a subgraph of the closure of~$T$.

\subsection{Rankwidth}
Introduced by \citet{OS06}. For a definition, see \cite{OS06} or \cite{HOSG08}.

\subsection{Width}
Consider a linear ordering of the vertices of a graph~$G$ and call its \emph{order} the maximum over all vertices~$v$ of the neighbors of~$v$ preceding~$v$ in the ordering. The \emph{width} of~$G$ a graph is the minimum order of all linear orderings of the vertices of~$G$.

Introduced by \citet{Fre82}.

\section{Proofs}
% The label convention:

% For the parameter abbreviations, see the ones used in graph_params.tex

% Equality: e-[parameter 1]-[parameter 2]
% Upper bounds: b-[bounding parameter]-[bounded parameter]
% Incomparability: i-[parameter 1]-[parameter 2]
% Unboundedness: un-[not bounding parameter]-[not bounded parameter]
\subsection{Equality}
\begin{lemma}[\cite{RS91}]
  Let~$\beta$ be the branchwidth and~$\omega$ be the treewidth. $\max\{\beta, 2\} \leq \omega + 1\leq \max\{3\beta/2, 2\}$.
\end{lemma}

\begin{lemma}[\cite{journal-KT96}]
  Let~$d$ be the degeneracy and~$w$ the width. We have~$d = w$.
\end{lemma}

\begin{lemma}
  Let~$d$ be the degeneracy and~$c$ the subgraph-cutset-number. We have~$d \geq c \geq \lfloor d/4 \rfloor + 1$.
\end{lemma}
\begin{proof}
  Let~$G$ be a graph with degeneracy~$d$. For the first inequality, consider any subgraph~$G'$ of~$G$. Since~$G$ is $d$-degenerate, there is a vertex~$v$ in~$G'$ with $\deg_{G'}(v)\leq d$. Cutting $v$'s neighbors yields a disconnected graph.

  For the second inequality, we use a result of~\citet[Korollar 1]{Mad72}. Namely, every graph~$G$ with $n$~vertices and $m$~edges that fulfills~$m > (2k - 3)(n - k + 1)$ and~$n \geq 2k - 1$ contains a $k$-vertex-connected subgraph. Consider any subgraph~$G'$ of a graph with degeneracy~$d$ such that~$G'$ has minimum degree~$d$. This subgraph has~$n \geq d$ vertices and at least~$dn/2$ edges. Let us show that choosing any positive~$k \leq \lfloor d/4 \rfloor + 1$ fulfills both conditions on~$G$ of the above statement. This is clear for the second condition, as~$n \geq d \geq \lfloor d/2 \rfloor + 1 \geq 2\lfloor d/4 \rfloor + 1 \geq 2k - 1$. Since~$G'$ has minimum degree~$d$, it contains at least~$dn/2$ edges, and, hence we need to show~$dn/2 > (2k - 3)(n - k + 1)$. Since~$n - k + 1$ is positive and less than~$n$, it suffices to show~$d/2 > 2k-3$ which is implied by~$\lfloor d/4\rfloor \geq k-1$. Hence, $G'$ contains a $k$-vertex-connected subgraph, and any of its supergraphs has subgraph-cutset-number at least~$k$.
\end{proof}

\begin{lemma}[\cite{OS06}]\label{e-rwd-cwd}
  Let~$r$ be the rankwidth and~$q$ the cliquewidth. We have~$r \leq q \leq 2^{1 + r} - 1$.
\end{lemma}

\begin{lemma}\label{e-arb-deg}
  Let~$a$ be the arboricity and~$d$ the degeneracy. We have~$a \leq d \leq 2a -1$.
\end{lemma}
\begin{proof}
  If the graph~$G$ has degeneracy~$d$, then there is an acyclic orientation with outdegree at most~$d$. Injectively mapping $d$ edge sets to the outgoing edges of a vertex yield a partition into~$d$ forests because, if one of the edge sets contained a cycle, then there were two oriented paths from one vertex to another, which means that we would have assigned the same edge set to two outgoing edges of a vertex; absurd. Hence the arboricity is at most~$d$.

  In the other direction, any subgraph of a graph~$G$ with arboricity~$a$ has also arboricity at most~$a$. Thus, any subgraph has average degree at most~$2a(n-1)/n$ and, hence, contains a vertex of degree at most~$2a - 1$. Hence the degeneracy of~$G$ is at most~$2a - 1$.  
\end{proof}

\subsection{Bounds}
% Please move proofs from this section to the Strict Bounds section or to the Equality section as soon as there is a corresponding unboundedness proof or bound in the other direction.

\begin{lemma}[\cite{conf-FR05}]\label{b-mxd-acn}
  The acyclic chromatic number~$\acn$ is upper bounded by the maximum degree~$\mxd$ (for every graph with~$\mxd>4$). We have~$\acn\leq\mxd(\mxd-1)/2$.
\end{lemma}
\begin{lemma}\label{b-hid-acn}
  The acyclic chromatic number~$\acn$ is upper bounded by the \mbox{$h$-index}~$h$. We have~$\acn\leq h(h+1)/2$.
\end{lemma}
\begin{proof}
  Let~$G$ be a graph and let~$H$ be the set of vertices of degree more than~$h$ in~$G$.
  Then the maximum degree of~$G-H$ is at most~$h$.
  Thus, by \autoref{b-mxd-acn}, $G-H$ can be acyclically colored with at most~$h(h-1)/2$ colors.
  Finally, assign each vertex in~$H$ a new color. Thus, $G$ can be acyclically colored with at most $h(h-1)/2+h=h(h+1)/2$ colors.
\end{proof}

\begin{lemma}[\cite{journal-AB78}]\label{b-gen-acn}
  The acyclic chromatic number~$\acn$ is upper bounded by the genus~$\gen$. We have~$\acn\leq 4\gen+4$.
\end{lemma}

\begin{lemma}[\cite{journal-EJ13}]\label{b-acn-box}
  The boxicity~$\boxicity$ is upper bounded by the acyclic chromatic number~$\acn$ (for every graph with~$\acn>1$). We have~$\boxicity\leq \acn(\acn-1)$.
\end{lemma}

\begin{lemma}[\cite{Dou92,DW09}]\label{b-ml-ddp}
  The max-leaf number~$\ml$ upper bounds the distance to disjoint paths~$d$. We have $d \leq \ml - 1$.   
\end{lemma}
\begin{proof}
  Let $n$ be the number of vertices. From the proof of Corollary 2 in
  ref.~\cite{Dou92} it follows that $n = \ml + \gamma_c$ where
  $\gamma_c$ is the smallest size of a connected dominating set.
  Clearly, $n = d + f$ where $f$ is the largest size of a vertex
  subset inducing a disjoint set of paths. By Theorem 4 in
  ref.~\cite{DW09} we have $\gamma_c \leq f - 1$ and, hence,
  $\ml - 1 \geq d$.
\end{proof}

\begin{lemma}[\cite{ABC11}]\label{b-mxd-box}
  The boxicity~$b$ is upper bounded by the maximum degree~$\Delta$. We have~$b \leq O(\Delta \log^2\Delta)$.
\end{lemma}

\begin{lemma}[\cite{CS07}]\label{b-tw-box}
  The treewidth~$\omega$ upper bounds the boxicity~$b$. We have~$b \leq \omega + 2$.
\end{lemma}

\begin{lemma}[\cite{BK10}]\label{b-avgdist-gir}
  The average distance~$d$ upper bounds the girth~$g$. We have~$d \geq ng/(4n-4)$.
\end{lemma}

\begin{lemma}[\cite{DW08}]\label{b-ml-fvs}
  The max leaf number~$m$ upper bounds the feedback vertex set size~$f$. We have~$f \leq m$.
\end{lemma}

\begin{lemma}\label{b-box-cho}
  The boxicity~$b$ upper-bounds the chordality~$c$. We have~$c \leq b$.
\end{lemma}
\begin{proof}
  The bound follows from the fact that interval graphs are chordal.
  Strictness is shown, for example, by \citet{CFM11} (even for bipartite graphs).
\end{proof}


\begin{lemma}\label{b-dig-box}
  The distance~$i$ to an interval graph upper bounds the boxicity~$b$. We have~$b \leq i + 1$.
\end{lemma}
\begin{proof}
  Let~$G = (V, E)$ be a graph and let~$D \subseteq V$ be a vertex set such that~$|D| = i$ and $G - D$ is an interval graph. We define an extension of~$G - D$ and intersect it with an interval graph~$G_v$ for each vertex~$v \in D$ to obtain~$G$ as follows. The extension~$G' = (V, E')$ is obtained from~$G$ by making each vertex~$u \in D$ universal, that is, by making every vertex in~$V$ adjacent to~$u$. Since adding universal vertices to an interval graph yields an interval graph, $G'$ is an interval graph. For each vertex~$v \in D$ we define~$G_v = (V, E_v)$: $G_v$ consists of a clique with vertex set~$V \setminus \{v\}$ and an edge~$\{u, v\}$ for each vertex~$u$ incident to~$v$. Each~$G_v$ is clearly an interval graph. It is also not hard to see that~$E = E' \cap \bigcap_{v \in D}E_v$. Hence the boxicity of~$G$ is at most~$i + 1$.
\end{proof}

\begin{lemma}\label{b-dch-cw}
  The distance~$c$ to a cograph upper bounds the cliquewidth~$q$. We have~$q \leq 2^{3 + c} - 1$.
\end{lemma}
\begin{proof}
  Let~$G = (V, E)$ be a graph and~$D \subseteq V$ be a vertex set such that~$|D| = c$ and~$G - D$ is a cograph. Since cographs have cliquewidth at most~2~\cite{CO00}, by \autoref{e-rwd-cwd} we have that the rankwidth of~$G - D$ is at most~2. Since~$G - D$ is obtained from~$G$ by~$c$ vertex deletions and deleting a vertex decreases the rankwidth by at most~1~\cite{HOSG08}, we have that~$G$ has rankwidth at most~$2 + c$. Applying again \autoref{e-rwd-cwd}, the cliquewidth of~$G$ is at most~$2^{3 + c} - 1$.
\end{proof}

\begin{lemma}\label{b-acn-deg}
  The acyclic chromatic number~$a$ upper bounds the degeneracy~$d$. We have~$d \leq 2\binom{a}{2} - 1$.
\end{lemma}
\begin{proof}
  If the graph~$G$ has acyclic chromatic number~$a$, then there is a partition of its edges into at most~$\binom{a}{2}$ forests (one for each pair of colors). Hence, the arboricity of~$G$ is at most~$\binom{a}{2}$. The statement follows from \autoref{e-arb-deg}.
\end{proof}

\begin{lemma}\label{b-gen-fes}
  The feedback edge set number~$f$ upper bounds the genus~$g$. We have $g \leq f$.
\end{lemma}
\begin{proof}
  Fix a spanning forest. This graph has genus~0. Add the remaining
  edges one-by-one. After adding the $i$th edge, the current graph is
  embeddable in a surface of genus~$i$, because we can introduce a new
  handle for each edge.
\end{proof}

\begin{lemma}\label{b-fvs-dch}
  The feedback vertex set~$f$ upper bounds the distance to a chordal
  graph~$c$. We have $c \leq f$.
\end{lemma}
\begin{proof}
  A tree is chordal.
\end{proof}

\subsection{Strict Bounds}

\begin{lemma}[\cite{journal-DZ99}]\label{b-tw-acn}
  The acyclic chromatic number~$\acn$ is strictly upper bounded by the treewidth~$\omega$. We have~$\acn\leq \omega+1$.
\end{lemma}
\begin{proof}
  Starting in the root of a width-$\omega$ tree decomposition, assign colors to vertices in such a way that, for each bag~$B$, the vertices in~$B$ have pairwise different colors.
  Since, for each cycle~$C$, there is some bag containing at least three vertices of~$C$, all cycles have at least three colors.
  Strictness follows from $n \times n$-grids having treewidth $n+1$ and acyclic chromatic number three.
\end{proof}

\begin{lemma}
  The maximum degree~$\mxd$ is strictly upper bounded by the bandwidth~$\bw$. We have~$\mxd\leq 2\bw$.
  \label{b-bw-mxd}
\end{lemma}
\begin{proof}
  Let~$v$ be a vertex in a graph~$G$ with bandwidth~$\bw$. There are at most~$\bw$ neighbors of~$v$ to the ``right'' in a bandwidth layout of~$G$ and equally as many to the ``left''.

  The bound is strict by a $n \times n$-grid example: Maximum degree is four, but treewidth is~$n + 1$ and treewidth is a lower bound for the bandwidth.
\end{proof}

\begin{lemma}[\cite{CO00}]\label{b-tw-clw}
  The cliquewidth of a graph is upper bounded by~$2^{\omega + 1} + 1$ where $\omega$ is its treewidth. This bound is strict.
\end{lemma}
Involved proof. The bound is strict by a clique example.

\begin{lemma}\label{b-hid-deg}
  The $h$-index~$h$ strictly upper bounds degeneracy~$d$. We have~$d \leq h$.
\end{lemma}
\begin{proof}
  Construct a ``degeneracy ordering'' as follows. First, enumerate all vertices of degree~$\leq h$ in any order, then enumerate all remaining vertices in any order (observe that there are at most~$h$ vertices of degree more than~$h$).
  
  Strictness follows from a disjoint union of stars.
\end{proof}

\begin{lemma}\label{b-ml-bw}
  The max leaf number~$\ml$ strictly upper bounds the bandwidth~$\bw$.
\end{lemma}
\begin{proof}
  We show $\bw\leq 2\ml$. Let~$T$ be a BFS tree of the graph from some vertex~$v$ and let~$L_i$ denote the vertices of distance~$i$ to~$v$ (horizontal ``layers'' of~$T$).
  %
  We show for all~$i$ that~$T$ has at least~$|L_i|$ leaves. For the last layer~$i_{\operatorname{max}}$, this is trivially true. Now, assume that~$T$ has at least~$|L_{i+1}|$ leaves. If~$|L_i|\leq|L_{i+1}|$, then~$T$ also has at least~$|L_i|$ leaves. Assume~$|L_i|>|L_{i+1}|$. However, since~$T$ is a tree, no vertex in~$L_{i+1}$ is adjacent to two vertices in~$L_i$. Thus, at least~$|L_i-L_{i+1}|$ vertices in~$L_i$ are leaves in~$T$. Thus, $T$~has at least~$|L_i|$ leaves, implying that~$\max_i\{|L_i|\}\leq\ml$.

  Now, by the definition of~$L_i$, the layout that simply puts all vertices in~$L_i$ before each vertex in~$L_{i+1}$ for all~$i$ has bandwidth at most~$\max_i\{|L_i|+|L_{i+1}|\}\leq 2\max_i\{|L_i|\}\leq 2\ml$.


  Strictness follows from long caterpillars with degree-three backbone vertices.
\end{proof}

\begin{lemma}\label{b-mcc-is}
  The minimum clique cover number~$c$ strictly upper bounds the independence number~$\alpha$. 
\end{lemma}
% This proof is due Gerhard Woeginger.
\begin{proof}
  Let~$G$ be a graph. Then,~$c(G) = \chi(\overline{G})$ where~$\chi$ is the chromatic number and~$\alpha(G) = \omega(\overline{G})$ where~$\omega$ is the clique number. Since there is a family of graphs with unbounded~$\chi$ and~$\omega = 2$ (see Mycielski graph), there does not exist a function~$f$ such that for all graphs~$G$ it holds that~$c(G) = \chi(\overline{G}) \leq f(\omega(\overline{G})) = f(\alpha(G))$. On the other hand, it is trivial that~$\chi \pargeq \omega$, implying $c\pargeq\alpha$.
\end{proof}

\begin{lemma}\label{b-td-pw}
  The treedepth~$t$ strictly upper bounds the pathwidth~$p$. We have
  $p \leq t$.
\end{lemma}
\begin{proof}
  Let~$G$ be a graph and $T$ be a tree of height $t(G)$ such that $G$
  is a subgraph of~$T$. Construct a path-decomposition as follows. For
  each leaf~$v \in V(T)$, introduce a bag containing all ancestors
  of~$v$ in~$T$. Two bags are adjacent in the path-decomposition if
  the corresponding leaves~$u, v$ have the property that there is no
  third leaf occuring between $u$ and~$v$ in a post-order traversal of~$T$.
  It is not hard to check that this indeed results in a
  path-decomposition and that each edge is contained in a bag.

  Strictness follows from the fact that a path with $n$ vertices has
  pathwidth~$1$ and treedepth at least~$\lfloor \log n \rfloor$.
\end{proof}

\begin{lemma}\label{b-vc-td}
  The minimum vertex cover size~$v$ strictly upper bounds the
  treedepth~$t$. We have $t \leq v + 1$.
\end{lemma}
\begin{proof}
  Let~$G$ be a graph and~$U \subseteq V(G)$ a vertex cover. Construct
  a tree~$T$ with vertex set~$V(G)$ by making~$U$ into a path, picking
  an endpoint~$u$ of the path, and making each vertex in
  $V(G) \setminus U$ adjacent to~$u$. Clearly, $G$ is a subgraph of
  the closure of~$T$ and hence the treedepth of~$G$ is at most~$|U| + 1$.

  Strictness can be seen as follows. Consider a rooted tree~$T$ with
  $n$ leaves, each of which has distance exactly two from the root.
  Tree~$T$ has minimum vertex cover size~$n$ but treedepth~$2$.
\end{proof}

\subsection{Unboundedness}
% Please move proofs from this section to the Strict Bounds section or to the Incomparability section as soon as there is a corresponding bound or unboundedness proof in the other direction.

\subsection{Incomparability}
\begin{lemma}\label{i-box-cw}
  Boxicity and cliquewidth are incomparable.
\end{lemma}
\begin{proof}
  Consider a square grid with side-length~$n$. Its cliquewidth is~$n+1$~\cite{GR00}. However, such a grid has boxicity two: Arrange the grid in two-dimensional space such that each edge is either perpendicular or parallel to the coordinate axis. Tilt the grid by 45 degrees. Then, replace each vertex by a square such that it overlaps on the edges with the squares of the vertex' neighbor and only with those squares.

  For the other direction, consider a graph~$G$ which is a clique with a perfect matching~$\{\{v_i^1, v_i^2\} \mid 1 \leq i \leq n / 2 \}$ removed. This graph is a co-graph: we show that there is no induced path~$P_4$ on four vertices. Consider any four vertices. The number of edges induced by these four vertices is minimized if they correspond to two edges of the removed matching. However, they still induce at least four edges and, thus, there is no induced~$P_4$. It follows that~$G$ has cliquewidth two. However, $G$ has boxicity at least~$n/2$: Let~$G_i = (V(G), E_i)$, $1 \leq i \leq b$, be interval graphs such that~$E(G) =\bigcap_{i = 1}^b E_i$. All edges of~$G$ have to occur in each~$E_i$,~$1 \leq i \leq b$. However, no two of the matching edges, say~$\{v_e^1, v_e^2\}, \{v_f^1, v_f^2\}$ may not occur in~$E_i$, since, otherwise,~$v_e^1, v_f^1, v_e^2, v_f^2$ is an induced cycle in~$G_i$ and, hence, this graph is not an interval graph. Thus, the boxicity is at least~$n/2$.
\end{proof}

\begin{lemma}\label{i-ddp-td}
  Distance to disjoint paths and treedepth are incomparable.
\end{lemma}
\begin{proof}
  To see that the distance to disjoint path does not upper bound the
  treedepth, consider a path with $n$ vertices, which has treedepth at
  least~$\log n$.

  For the other direction, consider a collection of $n$
  vertex-disjoint triangles.
\end{proof}

\begin{lemma}\label{i-deg-tw}
  Cliquewidth and degeneracy are incomparable.
\end{lemma}
\begin{proof}
  Since a clique has cliquewidth~$2$, cliquewidth does not upper bound degeneracy.
  
  On the other hand, square grids with side-length~$n$ have cliquewidth exactly~$n+1$~\cite{GR00}; but have degeneracy~$4$.
\end{proof}

\begin{lemma}\label{i-hid-tw}
  Treewidth and~$h$-index are incomparable.
\end{lemma}
\begin{proof}
  The disjoint union of stars proves that the~$h$-index can be arbitrarily large whereas the treewidth is~$1$.
  
  A grid proves the other direction.
\end{proof}

\begin{lemma}\label{i-ml-td}
  Max leaf number and treedepth are incomparable.
\end{lemma}
\begin{proof}
  Consider the set of paths and the set of stars.
\end{proof}

\bibliographystyle{abbrvnat}
\bibliography{main}
\end{document}

%%% Local Variables:
%%% mode: latex
%%% TeX-master: t
%%% End:
