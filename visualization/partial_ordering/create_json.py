from xml.dom import minidom
import numpy as np
import networkx as nx
from itertools import combinations, permutations
import json
from argparse import ArgumentParser

def parse_arguments():
    parser = ArgumentParser(description='Create JSON for visualization')
    parser.add_argument('--input', help='data in XML format',
                        required=True)
    parser.add_argument('--output', help='output file',
                        required=True)
    args = parser.parse_args()
    return args

# mapps parameters to integers for use as index in numpy array
def mapping(parameters):
    m = {}
    m_rev = {}
    count = 0
    for p in parameters:
        m[p] = count
        m_rev[count] = p
        count += 1
    return m,m_rev

def build_relations_(relations,m):
    size = len(m)
    B = np.zeros((size,size))
    N = np.zeros((size,size))
    N_ = []
    for (p1,r,p2) in relations:
        m1 = m[p1]
        m2 = m[p2]
        if r == '>=':
            B[m1,m2] = 1
        elif r == 'not >=':
            N[m2,m1] = 1
            N_.append((m2,m1))
        elif r == '>':
            B[m1,m2] = 1
            N[m2,m1] = 1
            N_.append((m2,m1))
    return B,N,N_

def build_relations(relations,m):
    size = len(m)
    B,N,N_ = build_relations_(relations,m)
    B_trans = transitive_closure(B,size)
    return B,B_trans,N,N_

def transitive_closure(B,size):
    G = nx.DiGraph(B)
    H = nx.DiGraph([(u,v,{'d':l}) for u,adj
                in nx.floyd_warshall(G).items() for v,l in adj.items() if l > 0 and l < float('inf')])
    B_ = np.zeros((size,size))
    for (n1,n2) in H.out_edges_iter():
        B_[n1,n2] = 1
    return B_

def lemma2(B,N,N_):
    N__ = N_[:]
    for (a,b) in N_:
        for g in range(B.shape[1]):
            if g == a or g == b:
                continue
            if B[a,g] == 1:
                if N[g,b] == 0:
                    N__.append((g,b))
                N[g,b] = 1
    return N,N__

def lemma3(B,N,N_):
    N__ = N_[:]
    for (g,b) in N_:
        for a in range(B.shape[0]):
            if a == g or a == b:
                continue
            if B[a,b] == 1:
                if N[g,a] == 0:
                    N__.append((g,a))
                N[g,a] = 1
    return N,N__

def complete_relations(relations,m):
    B,B_trans,N,N_ = build_relations(relations,m)
    current_size = len(N_)
    while True:
        N,N_ = lemma2(B_trans,N,N_)
        N,N_ = lemma3(B_trans,N,N_)
        new_size = len(N_)
        if new_size == current_size:
            break
        current_size = new_size
        
    return B,B_trans,N,N_

# mapping for the coloring
def classes(B,N,m_rev,parameters):
    m = {}
    for par2 in range(B.shape[0]):
        n = {}
        for par1 in range(B.shape[1]):
            if par1 == par2:
                continue
            par1_ = parameters[m_rev[par1]]
            if B[par2,par1] and N[par1,par2]:
                n[par1_] = "class1"
            elif B[par2,par1] and not N[par1,par2]:
                n[par1_] = "class2"
            elif not B[par2,par1] and N[par1,par2]:
                n[par1_] = "class3"
            elif B[par1,par2] and not N[par2,par1]:
                n[par1_] = "class4"
            elif not B[par1,par2] and N[par2,par1]:
                n[par1_] = "class5"
            elif B[par1,par2] and N[par2,par1]:
                n[par1_] = "class6"
            elif not B[par1,par2] and not B[par2,par1] and not N[par1,par2] and not N[par2,par1]:
                n[par1_] = "class7"
        m[parameters[m_rev[par2]]] = n
    return m

def links(B,m_rev,parameters):
    links_ = []
    for i in range(B.shape[0]):
        for j in range(B.shape[1]):
            if i == j:
                continue
            if B[i,j]:
                links_.append({"source" : parameters[m_rev[i]], "target" : parameters[m_rev[j]]})
    return links_

def nodes(parameters):
    d = []
    for v in parameters.values():
        d.append(v)
    return d



def main():
    args = parse_arguments()

    parameters = {}

    xmldoc = minidom.parse(args.input)

    for p in xmldoc.getElementsByTagName('Parameter'):
        parameters[p.attributes['id'].value] = p.attributes['name'].value

    relations = []

    for p in xmldoc.getElementsByTagName('parrelation'):
        relations.append((p.attributes['param1'].value,p.attributes['rel'].value,p.attributes['param2'].value))

    m,m_rev = mapping(parameters)

    B,B_trans,N,N_ = complete_relations(relations,m)

    graph = {}
    graph["nodes"] = nodes(parameters)
    graph["links"] = links(B,m_rev,parameters)
    graph["mapping"] = classes(B_trans,N,m_rev,parameters)
 
    json.dump(graph,open(args.output,'w'))


if __name__ == "__main__":
    main()